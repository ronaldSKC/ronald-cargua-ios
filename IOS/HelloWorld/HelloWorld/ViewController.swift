//
//  ViewController.swift
//  HelloWorld
//
//  Created by Ronald Cargua on 10/1/19.
//  Copyright © 2019 Ronald Cargua. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var helloWorldLabel: UILabel!
    @IBOutlet weak var newTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newTextField.keyboardType = .emailAddress
        
    }

    @IBAction func changeTextPressed(_ sender: Any) {
        helloWorldLabel.text = newTextField.text
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSecondViewController" {
            let destinationView = segue.destination as! SecondViewController
            destinationView.customTitle = newTextField.text
        }
    }
    
}

