

import UIKit

class SecondViewController: UIViewController {

    
    @IBOutlet weak var viewTitleLabel: UILabel!
    var customTitle : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTitleLabel.text = customTitle

    }

    @IBAction func changeButtonPressed(_ sender: Any) {
        viewTitleLabel.text = "New Title"
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil) 
    }
}
