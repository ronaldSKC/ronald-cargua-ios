import UIKit

var str = "Hello, playground"
var number = 5
var numbers = [1,2,3,4,5]
let sum = numbers.reduce(0,+)
let dictionary = [
    "name":"Ronald",
    "lastname": "Cargua",
    "age": "23"]
dictionary["name"]

let temperature = 16

switch temperature{
case 1...8 where temperature % 2 == 0:
    print("It's cold! ")
case 9...12:
    print("Not too cold")
case 13...17:
    print("Just about right")
default:
    print("---")
}

func myFunction(){
    print("Hey!")
}
myFunction()

func square(number: Int) -> Int {
    return number * number
}
square(number: 7)

func mensaje (mail: String, mensaje: String){
    print("Mail: \(mail)  Mensaje: \(mensaje)")
}

mensaje(mail: "Hola", mensaje: "Ronald")
func square2(_ number: Int) -> Int{
    return number * number
}
square2(4)

func sSum (n1: Int, n2: Int, n3: Int = 0) -> Int {
    return n1 + n2 + n3
}
sSum(n1: 1, n2: 2)
sSum(n1: 3, n2: 4, n3: 2)

func mult (n1: Int, n2:Int) -> Int {
    return n1 * n2
}

func takes (n1: Int, n2: Int) -> Int {
    return n1 - n2
}

func opp (n1: Int, n2: Int, operation: ((Int, Int) -> Int)) -> Int{
    return operation(n1, n2)
}

opp(n1: 1, n2: 5, operation: takes(n1:n2:))
opp(n1: 1, n2: 5, operation: mult(n1:n2:))

func specialSum<T: Numeric> (n1: T, n2: T) -> T {
    return n1 + n2
}

// Classes & Structs

struct Car {
    var maxSpeed:Int
    var brand:String
// NO!
//   mutating func incrementMaxSpeed() {
//        maxSpeed += 10
//    }
}

var carA = Car(maxSpeed: 200, brand: "VW")
carA.brand = "Audi"

class Person {
    let name: String
    let lastName: String
    let nId: String
    init(name: String, lastName: String, nId: String) {
        self.name = name
        self.lastName = lastName
        self.nId = nId
    }
    func doSomething (){
        print("I'm a person")
    }
}
let personA = Person(name: "Ronald", lastName: "Cargua", nId: "123456789")

protocol Friendable {
    var friend : Person?{ get set }
    func addFriend(friend : Person)
}

class Employe: Person, Friendable{
    let jobTitle : String
    var salary: Double?
    var friend: Person?
    init(name: String, lastName: String, nId: String, jobTitle: String) {
        self.jobTitle = jobTitle
        super.init(name: name, lastName: lastName, nId: nId)
    }
    func addFriend(friend: Person) {
        self.friend = friend
    }
    func singleFuntion(var1: Int){
        
    }
    func singleFuntion(var1: String) {

    }
    override func doSomething (){
        print("I'm a enployed")
    }
}

//private
//fileprivate


func polimorphic (person: Person){
    print(person.name)
}
let employee = Employe(name: "Ronald", lastName: "Cargua", nId: "123456789", jobTitle: "Student")
polimorphic(person: employee)


var myOptical: Int?
print(myOptical)
myOptical = 10
print(myOptical)

func somethingCool (number: Int){
    print("Number is: \(number)")
}

somethingCool(number: myOptical ?? 0)

func otherSomething (number: Int?){
    if let noNumber = number, noNumber % 2 == 0 {
        print("Number is \(noNumber)")
    }
}
myOptical = nil
otherSomething(number: myOptical)

func moreOptional (number: Int?){
    guard let noNumber = number else {
        print("Null!!!!")
        return
    }
    print(noNumber)
}
moreOptional(number: myOptical)

myOptical = 20
print(myOptical)
print(myOptical!)
